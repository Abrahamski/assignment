package task2Page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Task2Page {

private WebDriver driver;
	
	public Task2Page (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getSearchInputField() {
		return driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input"));
	}
	
	public void setSearch(String value){
		WebElement el = this.getSearchInputField();
		el.clear();
		el.sendKeys(value);
		el.submit();
		//el.sendKeys(Keys.ENTER);
	}
	
	
	public WebElement resultNumber() {
		return driver.findElement(By.id("result-stats"));
		
	}
}
