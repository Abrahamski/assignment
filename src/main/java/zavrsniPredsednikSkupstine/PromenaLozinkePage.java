package zavrsniPredsednikSkupstine;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import zavrsniAdmin.Utils;

public class PromenaLozinkePage {

private WebDriver driver;
	
	public PromenaLozinkePage (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getPromenaLozinke() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[@href='/promenaLozinke']"), 10);
	}
	
//	public void navigateToPocetna(){
//		driver.navigate().to("localhost:8080/logovanje");
//	}
	
	public void navigateToPromenaLozinke(){
		driver.navigate().to("localhost:8080/promenaLozinke");
	}
	
	public WebElement getFormaZaPromenuLozinke() {
		return Utils.waitForElementPresence(driver, By.className("promenaLozinke"), 10);
	}
	
	public WebElement getPoljeStaraLozinka() {
		return Utils.waitForElementPresence(driver, By.xpath("//input[@placeholder='Stara lozinka']"), 10);
	}
	
	public void setStaraLozinka(String staraLozinka) {
		WebElement staraLozinkaInput = getPoljeStaraLozinka();
		staraLozinkaInput.clear();
		staraLozinkaInput.sendKeys(staraLozinka);
	}
	
	public WebElement getPoljeNovaLozinka() {
		return Utils.waitForElementPresence(driver, By.id("novaLozinka"), 10);
	}
	
	public void setNovaLozinka(String novaLozinka) {
		WebElement novaLozinkaInput = getPoljeNovaLozinka();
		novaLozinkaInput.clear();
		novaLozinkaInput.sendKeys(novaLozinka);
	}
	
	public WebElement getPoljePotvrdaLozinke() {
		return Utils.waitForElementPresence(driver, By.id("potvrdaNoveLozinke"), 10);
	}
	
	public void setPotvrdaLozinke(String potvrdaLozinke) {
		WebElement potvrdaLozinkeInput = getPoljePotvrdaLozinke();
		potvrdaLozinkeInput.clear();
		potvrdaLozinkeInput.sendKeys(potvrdaLozinke);
	}
	
	public WebElement getPromeniteLozinkuBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@type='submit']"), 10);
	}
	
	public void setPromeniLozinku(String staraLozinka, String novaLozinka, String potvrdaLozinke) {
		setStaraLozinka(staraLozinka);
		setNovaLozinka(novaLozinka);
		setPotvrdaLozinke(potvrdaLozinke);
		getPromeniteLozinkuBtn().click();
	}
	
	public String getPogresnaLozinkaMsg() {
		return Utils.waitForElementPresence(driver, By.className("toast-error"), 10).getText();
	}
	
	public String getNeispravanFormatLozinkeMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[text()='Neispravna lozinka! Pogledajte napomenu.']"), 10).getText();
	}
	
	public String getNepoklapanjeLozinkiMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[text()='Lozinke se ne poklapaju!']"), 10).getText();
	}
	
	public String getUspesnoPromenjenaLozinkaMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='toast-container']/div/div"), 10).getText();
	}
}
