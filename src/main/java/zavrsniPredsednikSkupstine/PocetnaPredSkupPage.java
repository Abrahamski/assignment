package zavrsniPredsednikSkupstine;


import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import zavrsniAdmin.Utils;

public class PocetnaPredSkupPage {

	private WebDriver driver;
	private WebElement datePocetak;
	private WebElement dateZavrsetak;
	private WebElement glasZ;
	private WebElement glasB;
	
	public PocetnaPredSkupPage (WebDriver driver) {
		this.driver = driver;
	}
	
	public void navigateToPagePocetnaPredSkup(){
		driver.navigate().to("localhost:8080/pocetna");
	}
	
	public WebElement getStranicaBtn(String text) {
		return Utils.waitForElementPresence(driver, (By.xpath("//*[contains(text(),\"" + text + "\")]")), 10);

	}
	
	public WebElement getTabelaZgradaUKojojZivi() {
		return Utils.waitForElementPresence(driver, By.id("zgradaStanuje"), 10);
	}
	
	public WebElement getTabelaZgradeUKojimaJeVlasnik() {
		return Utils.waitForElementPresence(driver, By.id("zgradaVlasnik"), 10);
	}
	
	public void getStranicaZgradaUKojojZivi() {
		this.getTabelaZgradaUKojojZivi().findElement(By.xpath("//a[contains(text(),'Stranica')]")).click();
	}
	
	public void getStranicaZgradaUKojimaJeVlasnik() {
		this.getTabelaZgradeUKojimaJeVlasnik().findElement(By.xpath("//a[contains(text(),'Stranica')]")).click();
	}
	
	public void navigateToPageZgradaUKojojJeVlasnik(){
		driver.navigate().to("localhost:8080/zgrada/1/obavestenja");//proveriti url da li je 1 ili 2
	}
	
	public WebElement getTitleZgrada() {
		return Utils.waitForElementPresence(driver, By.cssSelector("div h2"), 10);
	}
	
	public WebElement getObavestenja() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[(contains(text(), 'Obavestenja'))]"), 10);
	}
	
	public WebElement getDodajObavestenjeBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@style='cursor: pointer;']"), 10);
	}
	
	public WebElement getFormaDodavanjaObavestenja() {
		return Utils.waitForElementPresence(driver, By.cssSelector("fieldset"), 10);
	}
	
	public WebElement getPolje() {
		return Utils.waitForElementPresence(driver, By.id("tekstObavestenja"), 10);
	}
	
	public void setPolje(String tekst) {
		WebElement tekstObavestenja = getPolje();
		tekstObavestenja.clear();
		tekstObavestenja.sendKeys(tekst);
	}
	
	public WebElement getPotvrdiBtn() {
		return Utils.waitToBeClickable(driver, By.xpath("//button[@id='dodajObavestenje']"), 10);
	}
	
	public void setDodajObavestenje(String tekst) {
		setPolje(tekst);
		getPotvrdiBtn().click();
	}
	
	public String getUspesnoDodatoObavestenjeMsg() {
		return Utils.waitForElementPresence(driver, By.className("toast-message"), 10).getText();
	}
	
	public WebElement getTabelaObavestenje() {
		return Utils.waitForElementPresence(driver, By.cssSelector("div:nth-child(3) div table tbody"), 10);
	}//---ovde je selektor za pronalazenje prvog obavestenja na testu mozda treba promeniti
	
	public void getIzmeniBtn() {
		this.getTabelaObavestenje().findElement(By.id("izmeniObavestenje")).click();
	}
	
	public void getOdustaniBtn() {
		this.getTabelaObavestenje().findElement(By.xpath("//span[@style='color:red']")).click();
	}
	
	public void getPoljeIzmeni() {
		getTabelaObavestenje().findElement(By.id("stariTekst")).click();
	}
	
	public void setPoljeIzmeni(String noviTekst) {
		WebElement noviTekstInput = getTabelaObavestenje().findElement(By.id("noviTekst"));
		noviTekstInput.clear();
		noviTekstInput.sendKeys(noviTekst);
	}
	
	public void getPotvrdiIzmenuBtn() {
		getTabelaObavestenje().findElement(By.xpath("//span[@style='color:green']")).click();
	}
	
	public void izmeniObavestenje(String noviTekst) {
		setPoljeIzmeni(noviTekst);
		getPotvrdiBtn();
	}
	
	public String getUspesnoIzmenjenoObavestenjeMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='Uspesno izmenjeno obavestenje']"), 10).getText();
	}
	
	public void getBrisiBtn() {
		this.getTabelaObavestenje().findElement(By.xpath("//span[text()='brisi']")).click();
	}
	
	public String getUspesnoObrisanoObavestenjeMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='Uspesno izbrisano obavestenje']"), 10).getText();
	}
	
	public WebElement getSastanciSkupstine() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[(contains(text(), 'Sastanci skupstine'))]"), 10);
	}
	
	public WebElement getDodajSastanakBtn() {
		return Utils.waitForElementPresence(driver, (By.xpath("//a[@href='/zgrada/1/sastanak/zakazi']")), 10);
	}
	
//	public static void main(String[] args) {
//	// Create object of SimpleDateFormat class and decide the format
//	 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm aa");
//	 
//	 //get current date time with Date()
//	 Date date = new Date();
//	 
//	 // Now format the date
//	 String date1= dateFormat.format(date);
//	}
	

    //Fill date as mm/dd/yyyy as 09/25/2013
	public void setPocetakSkupstine(String datumP, String vremeP) {
		datePocetak = driver.findElement(By.xpath("//*[@name='pocetakSkupstine']"));
		datePocetak.sendKeys(datumP);
		datePocetak.sendKeys(Keys.TAB);
		datePocetak.sendKeys(vremeP);
	}


    //Fill date as mm/dd/yyyy as 09/25/2013
	public void setZavrsetakSkupstine(String datumK, String vremeK) {
		dateZavrsetak = driver.findElement(By.xpath("//*[@name='zavrsetakSkupstine']"));
		dateZavrsetak.sendKeys(datumK);
		dateZavrsetak.sendKeys(Keys.TAB);
		dateZavrsetak.sendKeys(vremeK);
	}

    
	 
//	public final Date getPocetakSkupstine() {
//		return (Date) Utils.waitForElementPresence(driver, (By.tagName("pocetakSkupstine")), 10);
//	}
//	
//	public final void setPocetakSkupstine(Date pocetakDatum) {
//		Date datumPocetkaInput = getPocetakSkupstine();
//		datumPocetkaInput.clear();
//		datumPocetkaInput.sendKeys(pocetakDatum);
//	}
	
//	public WebElement getZavrsetakSkupstine() {
//		return Utils.waitForElementPresence(driver, (By.tagName("zavrsetakSkupstine")), 10);
//	}
//	
//	public void setZavrsetakSkupstine(String zavrsetakDatum) {
//		WebElement datumZavrsetkaInput = getZavrsetakSkupstine();
//		datumZavrsetkaInput.clear();
//		datumZavrsetkaInput.sendKeys(zavrsetakDatum);
//	}
	
	public WebElement getPotvrdiSastanakBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(@class, 'btn btn-primary')]"), 10);
	}
	
//	public void setDatume(String pocetakDatum, String zavrsetakDatum) {
//		setPocetakSkupstine(pocetakDatum);
//		setZavrsetakSkupstine(zavrsetakDatum);
//		getPotvrdiSastanakBtn().click();
//	}
	
	public String getSastanakUspesnoZakazanMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='Sastanak uspesno zakazan']"), 10).getText();
	}
	
	public WebElement getPronadjiSastanak(String text) {
		return Utils.waitForElementPresence(driver, (By.xpath("//*[contains(text(),\"" + text + "\")]")), 10);
	}
	
	public void setPregledajTackeSastankaBtn(String text) {
		getPronadjiSastanak(text).findElement(By.className(".operacije")).click();
	}
	
	public WebElement setPregledTackeBtn() {
		return Utils.waitForElementPresence(driver, (By.cssSelector("body > app-root > app-zgrada > div > div.col-md-12 > app-sastanci > div:nth-child(5) > div > table > tbody > tr:nth-child(5) > td > span.tacke > a > span")), 10);
	}
	
	public WebElement getPronadjiAnketu(String text) {
		return Utils.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + text + "\")]"), 10);
	}
	
	public void setPogledajAnketu(String text) {
		getPronadjiAnketu(text).findElement(By.xpath("//span[text()='Pregledaj anketu']")).click();
	}
	
	public WebElement getGlasZaZuto() {
		return Utils.waitToBeClickable(driver, By.xpath(""), 10);
	}
	
	public WebElement getGlasZaBeloo() {
		return Utils.waitToBeClickable(driver, By.xpath(""),10);
	}
	
	public void setGlasajZuto() {
		WebElement radioZ = driver.findElement(By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-izlistaj-ankete/div/app-pitanje/div/fieldset/div/div[1]/div[1]/div/div/label"));
	}
	
	public void setGlasajBelo() {
		WebElement radioB = driver.findElement(By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-izlistaj-ankete/div/app-pitanje/div/fieldset/div/div[2]/div[1]/div/div/label"));
	}
	
	public void setIzmeniSastanakBtn(String text) {
		getPronadjiSastanak(text).findElement(By.xpath("//a[@href='/zgrada/2/sastanci/izmena/13']")).click();
	}
	
//u testu probati sa poljima za unos datuma ako ne radi izmeniti zakomentarisano
//	public void setPoljeIzmeni(String noviTekst) {
//		WebElement noviTekstInput = getTabelaObavestenje().findElement(By.id("noviTekst"));
//		noviTekstInput.clear();
//		noviTekstInput.sendKeys(noviTekst);
//	}
//	
//	public void getPotvrdiIzmenuSastankaBtn() {
//		getTabelaObavestenje().findElement(By.xpath("//span[@style='color:green']")).click();
//	}
//	
//	public void izmeniObavestenje(String noviTekst) {
//		setPoljeIzmeni(noviTekst);
//		getPotvrdiBtn();
//	}
	
	public String getUspesnoIzmenjenSastanakMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='Sastanak uspesno izmenjen!']"), 10).getText();
	}
	
	public void getOtkaziSastanakBtn(String text) {
		this.getPronadjiSastanak(text).findElement(By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-sastanci/div[2]/div/table/tbody/tr[5]/td/span[2]")).click();
	}
	
	public String getSastanakOtkazanMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='sastanak uspesno otkazan']"), 10).getText();
	}
	
	public WebElement getPredloziTackeDnevnogReda() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[(contains(text(), 'Predlozi tacke dnevnog reda'))]"), 10);
	}
	
	public WebElement getPredloziDnevnogReda(String text) {
		return Utils.waitForElementPresence(driver, (By.xpath("//*[contains(text(),\"" + text + "\")]")), 10);
	}

	public WebElement getSelektPonudjenihSkupstina() {
		return Utils.waitForElementPresence(driver, By.className("custom-select"), 10);
	}
	
	public String getBududcuSkupstinu() {
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='1: Object']"), 10).getText();
	}
		
	public WebElement getDodajTackuBtn() {
		return Utils.waitForElementPresence(driver, By.cssSelector("body > app-root > app-zgrada > div > div.col-md-12 > app-tacke > div:nth-child(4) > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > button"), 10);
	}
	
	public String getUspesnoDodeljenaTackaMsg() {
		return Utils. waitForElementPresence(driver, By.xpath("//div[@aria-label='Tacka uspesno dodata u skupstinu']"), 10).getText();
	}
	
//	public String getUspesnoKreiranaSkupstinaMsg() {
//		return Utils.waitForElementPresence(driver, By.className("toast-message"), 10).getText();
//	}

	public WebElement getInputPolje() {
		return Utils.waitForElementPresence(driver, By.id("tekstPtdr"), 10);
	}

	public void setInputTacka(String input) {
		WebElement tackaInput = getInputPolje();
		tackaInput.clear();
		tackaInput.sendKeys(input);
	}

	public WebElement getPotvrdiTackuBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(@class, 'btn btn-primary')]"), 10);
	}

	public void dodajTacku(String input) {
		setInputTacka(input);
		getPotvrdiBtn().click();
	}

	public String getDodataTackaMsg() {
		return Utils.waitForElementPresence(driver, By.className("toast-message"), 10).getText();
	}

	public WebElement getDodatuTacku(String text) {
		return Utils.waitForElementPresence(driver, (By.xpath("//*[contains(text(),\"" + text + "\")]")), 10);
	}
	
	public WebElement getKvarovi() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[(contains(text(), 'Kvarovi'))]"), 10);
	}
	
	public WebElement getKvarovi(String text) {
		return Utils.waitForElementPresence(driver, (By.xpath("//*[contains(text(),\"" + text + "\")]")), 10);
	}
	
	public WebElement getDodajKvarBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@style='cursor: pointer;']"), 10);
	}
	
	public WebElement getPoljeMestoKvara() {
		return Utils.waitForElementPresence(driver, By.id("mesto"), 10);
	}
	
	public void setPoljeMestoKvara(String mesto) {
		WebElement mestoInput = getPoljeMestoKvara();
		mestoInput.clear();
		mestoInput.sendKeys(mesto);
	}
	
	public WebElement getPoljeOpisKvara() {
		return Utils.waitForElementPresence(driver, By.id("opis"), 10);
	}
	
	public void setPoljeOpisKvara(String opis) {
		WebElement opisInput = getPoljeOpisKvara();
		opisInput.clear();
		opisInput.sendKeys(opis);
	}
	
	public WebElement getIzaberiOdgovornoLice() {
		return Utils.waitForElementPresence(driver, By.id("odgovorno_lice"), 10);
	}
	
	public WebElement getOdustani() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[text()='Odustani']"), 10);
	}
	
	public String getIzabranoOdgovornoLice(String odgovornoLice) {
		return Utils.waitForElementPresence(driver, (By.xpath("//*[contains(text(),\"" + odgovornoLice + "\")]")), 10).getText();
	}
	
	
	
	public WebElement getIzaberiBtn() {
		return Utils.waitForElementPresence(driver, By.id("odgovorno_lice"), 10);
	}
	
	public WebElement getPrihvatiBtn() {
		return Utils.waitToBeClickable(driver, By.id("button_2"), 10);
	}
	///TACNOOOO-----------------------------------------------------------------------------------------------
	public WebElement getPronadjiOdgovornoLice(String naziv) {
		return Utils.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + naziv + "\")]"), 10);
	}
	
	public void setPrihvatiBtn(String naziv) {
		 getPronadjiOdgovornoLice(naziv).findElement(By.xpath("//button[text()='Prihvati']")).click();
	}
	//------------------------------------------------------------------------------------------------------------
	public void setKvar(String mesto, String opis, String naziv) {
		setPoljeMestoKvara(mesto);
		setPoljeOpisKvara(opis);
		getIzaberiBtn().click();
		setPrihvatiBtn(naziv);
	}
	
	public WebElement getSacuvajKvarBtn() {
		return Utils.waitToBeClickable(driver, By.xpath("//button[text()='Dodaj']"), 10);
	}
	
	public String getUspesnoSacuvanKvarMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='Kvar uspesno dodat']"), 10).getText();
	}

	public WebElement getPogledajKvar() {
		return Utils.waitForElementPresence(driver, (By.xpath("//a[@href='/zgrada/1/kvar/1']")), 10);
	}//proveriti da li je bas za tu zgradu url i href

	public void getBrisiKvarBtn() {
		getPogledajKvar().findElement(By.xpath("//span[text()='brisi]'")).click();
	}
	
	public String getUspesnoObrisanKvar() {
		return Utils.waitForElementPresence(driver, By.xpath("//div[@aria-label='Uspesno izbrisan kvar']"), 10).getText();
	}
	
	public WebElement getIzmeniOdgovornoLice() {
		return Utils.waitForElementPresence(driver, By.xpath("//span[contains(@class, 'lower-impact-text']"), 10);
	}
	
	public WebElement getPredsednik() {
		return Utils.waitToBeClickable(driver, By.xpath("//a[contains(@href, '/stanar/')]"), 10);
	}
}
