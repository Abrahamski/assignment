package zavrsniAdmin;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StanariPage {

	private WebDriver driver;

	public StanariPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getPregledBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(@class, 'btn btn-outline-primary')]"), 10);
	}
	
	public WebElement getPrikazStanara() {
		return Utils.waitForElementPresence(driver, By.id("prikaz"), 10); 
	}
	
	public String getBrojPrikaza10(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='10']"), 10).getText();			
	}
	
	public String getBrojPrikaza25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();			
	}
	
	public WebElement getTabelaStanara () {
		return Utils.waitForElementPresence(driver, By.tagName("tbody"), 10);
	}
	
	public List<WebElement> getRedoviTabele() {
		return this.getTabelaStanara().findElements(By.className("table-secondary"));
	}

	public boolean isStanarInTable(String name) {
		return Utils.isPresent(driver, By.xpath("//*[contains(text(),\"" + name + "\")]/../.."));
	}
	
	public void getNavigateToStanar() {
		 this.driver.navigate().to("http://localhost:8080/stanar/2");
	}
	
	public void getNavigateToZgrada() {
		this.driver.navigate().to("http://localhost:8080/zgrada/1/stanovi");
	}
	/**
	 * 
	 * Selekcija reda na osnovu broja indeksa. Prvo se selektuje <a> tag koji
	 * ima tekst kao ime, a zatim se pokupi njegov roditelj
	 * <td>(/../), a zatim roditelj od
	 * <td>sto predstavlja
	 * <tr>
	 * (/../)
	 * 
	 * @param name
	 *            ime
	 * 
	 * @return student row
	 * 
	 */
	public WebElement getStanarByName(String name) {
		return Utils.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + name + "\")]/../.."), 10);
	}
	
	public WebElement getSearchText(String text) {
		return Utils.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + text + "\")]/../.."), 10);
	}
	
	public WebElement getSearchPolje() {
		return Utils.waitForElementPresence(driver, By.id("filter"), 10);
	}
	
	public void setSearchPolje(String imePrezimeEmailSearch){
		WebElement imePrezimeEmailInput = getSearchPolje();
		imePrezimeEmailInput.clear();
		imePrezimeEmailInput.sendKeys(imePrezimeEmailSearch);
	}
	
	public boolean isTextInTable(String text) {
		return Utils.isPresent(driver, By.xpath("//*[contains(text(),\"" + text + "\")]/../.."));
	}
	
	public WebElement getSearchBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(@class, 'btn btn-primary')]"), 10);
	}

	public WebElement getStanarRowByImePrezime(String imePrezime) {
		return Utils.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + imePrezime + "\")]/../.."), 10);
	}
		
	public WebElement getZgradaLink() {
		return Utils.waitForElementPresence(driver, By.cssSelector("#vlasnikStanova > tbody > tr > td.col-md-9 > a"), 10);
	}
}
