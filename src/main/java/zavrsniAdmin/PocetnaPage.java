package zavrsniAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PocetnaPage {

private WebDriver driver;
	
	public PocetnaPage (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getPocetnaMeni() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[@href='/pocetna']"), 10);
	}
	
	public void navigateToPagePocetna(){
		driver.navigate().to("localhost:8080/pocetna");
	}
	
	
	public WebElement getZgradeMeni() {
		return Utils.waitForElementPresence(driver, By.xpath("/html/body/app-root/router-outlet/app-navbar-admin/nav/div/ul[1]/li[2]/a"), 10);
	}
	
	public WebElement getZgradeOpcije() {
		return Utils.waitForElementPresence(driver, By.cssSelector("#opcije > li:nth-child(1) > a"), 10);
	}
	
	public void navigateToPageZgrade(){
		driver.navigate().to("localhost:8080/zgrade");
	}
	
	public WebElement getStanariMeni() {
		return Utils.waitForElementPresence(driver, By.xpath("/html/body/app-root/router-outlet/app-navbar-admin/nav/div/ul[1]/li[3]/a"), 10);
	}
	
	public WebElement getStanariOpcije() {
		return Utils.waitForElementPresence(driver, By.cssSelector("#opcije > li:nth-child(2) > a"), 10);
	}
	
	public void navigateToPageStanari(){
		driver.navigate().to("localhost:8080/stanari");
	}

	//zatamljen deo
//	public WebElement getInstitucijeMeni() {
//		return Utils.waitForElementPresence(driver, By.xpath(), 10);
//	}
	
//	public WebElement getInstitucijeOpcije() {
//	return Utils.waitForElementPresence(driver, By.xpath(), 10);
//}
//	
//	public void navigateToPageInstitucije(){
//		driver.navigate().to("localhost:8080/");
//	}
	
	//public WebElement getFirmeMeni() {
		//return Utils.waitForElementPresence(driver, By.xpath(), 10);
	//}
	
//	public WebElement getFirmeOpcije() {
//		return Utils.waitForElementPresence(driver, By.cssSelector(), 10);
//	}
	
//	
//	public void navigateToPageFirme(){
//		driver.navigate().to("localhost:8080/");
//	}
	
	public WebElement getIzlogujSeBtn() {
		return Utils.waitForElementPresence(driver, By.cssSelector("body > app-root > router-outlet > app-navbar-admin > nav > div > ul.nav.navbar-nav.navbar-right.btn-outline-primary > li:nth-child(2) > button"), 10);
	}
	
	public void logOut() {
		getIzlogujSeBtn().click();
	}

}
