package zavrsniAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegistracijaStanaraPage {

	private WebDriver driver;

	public RegistracijaStanaraPage (WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getStanari() {
		return Utils.waitForElementPresence(driver, By.xpath("//a[@href='/stanari']"), 10);
	}

	public WebElement getEmailStanar() {
		return Utils.waitForElementPresence(driver, By.id("email"), 10);
	}

	public void setEmailStanar(String email) {
		WebElement emailInput = getEmailStanar();
		emailInput.clear();
		emailInput.sendKeys(email);
	}

	public WebElement getLozinkaStanar() {
		return Utils.waitForElementPresence(driver, By.id("lozinka"), 10);
	}

	public void setLozinkaStanar(String lozinka) {
		WebElement lozinkaInput = getLozinkaStanar();
		lozinkaInput.clear();
		lozinkaInput.sendKeys(lozinka);
	}

	public WebElement getImeStanar() {
		return Utils.waitForElementPresence(driver, By.id("ime"), 10);
	}

	public void setImeStanar(String ime) {
		WebElement imeInput = getImeStanar();
		imeInput.clear();
		imeInput.sendKeys(ime);
	}

	public WebElement getPrezimeStanar() {
		return Utils.waitForElementPresence(driver, By.id("prezime"), 10);
	}

	public void setPrezimeStanar(String prezime) {
		WebElement prezimeInput = getPrezimeStanar();
		prezimeInput.clear();
		prezimeInput.sendKeys(prezime);
	}


	public WebElement getRegistracijaBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@type='submit']"), 10);
	}
	
	public void registracijaStanar(String email, String lozinka, String ime, String prezime) {
		setEmailStanar(email);
		setLozinkaStanar(lozinka);
		setImeStanar(ime);
		setPrezimeStanar(prezime);
		getRegistracijaBtn().click();
	}

	public WebElement getResetBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@type='button']"), 10);
	}
	
	public void resetPodataka(String email, String lozinka, String ime, String prezime) {
		setEmailStanar(email);
		setLozinkaStanar(lozinka);
		setImeStanar(ime);
		setPrezimeStanar(prezime);
		getResetBtn().click();
	}

	public String getPraznoPoljeMsg() {
		return Utils.waitForElementPresence(driver, By.className("invalid-feedback"), 15).getText();
	}

	public String getNevalidnaLozinkaMsg() {
		return Utils.waitForElementPresence(driver, By.className("invalid-feedback"), 15).getText();
	}

	public String getUspesnaRegMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='toast-container']/div/div"), 15).getText();
	}

	public String getZauzetiEmailMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='toast-container']/div/div"), 15).getText();
	}

}
