package zavrsniAdmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginAdminPage {
	
	private WebDriver driver;
	
	public LoginAdminPage (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getEmailInput() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@placeholder='Email']"), 10);
	}
	
	public void setEmail(String email) {
		WebElement emailInput = getEmailInput();
		emailInput.clear();
		emailInput.sendKeys(email);
	}
	
	public WebElement getPasswordInput() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@placeholder='Lozinka']"), 10);
	}
	
	public void setPassword(String password) {
		WebElement passwordInput = getPasswordInput();
		passwordInput.clear();
		passwordInput.sendKeys(password);
	}
	
	public WebElement getUlogujSeBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@type='submit']"), 10);
	}
	//da li je dugme zasivljeno ili ne -driver.findElement(By.id("elementID")).isEnabled(); ili assertVisible u testKlasi
		
	public void navigateToPage(){
		driver.navigate().to("localhost:8080/logovanje");
	}
	
	public void login(String email, String password) {
		setEmail(email);
		setPassword(password);
		getUlogujSeBtn().click();
	}
	
	
	public String getNeispravnaLozinkaLoginMsg (){
		return Utils.waitForElementPresence(driver, By.tagName("strong"), 10).getText();
	}
	
	public String getNeispravanEmailLoginMsg (){
		return Utils.waitForElementPresence(driver, By.tagName("strong"), 10).getText();
	}
	
	public WebElement getEmailFromMenu() {
		return Utils.waitForElementPresence(driver, By.xpath("/html/body/app-root/router-outlet/app-navbar-admin/nav/div/ul[2]/li[1]/label"), 10);		
	}
	
	public String getMsgPostojiEmail(){
		return Utils.waitForElementPresence(driver,  By.className("toast-message"), 10).getText();
				
	}
	
	public WebElement getIzlogujSeBtn() {
		return Utils.waitForElementPresence(driver, By.cssSelector("body > app-root > router-outlet > app-navbar-admin > nav > div > ul.nav.navbar-nav.navbar-right.btn-outline-primary > li:nth-child(2) > button"), 10);
	}
	
	public void logOut() {
		getIzlogujSeBtn().click();
		navigateToPage();
	}

}