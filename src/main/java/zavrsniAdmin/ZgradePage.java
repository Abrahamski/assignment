package zavrsniAdmin;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ZgradePage {
	
private WebDriver driver;
	
	public ZgradePage (WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement getZgrade(){
		return Utils.waitForElementPresence(driver, By.cssSelector("div form"), 10);
	}
	
	public WebElement getMesto(){
		return Utils.waitForElementPresence(driver, By.xpath("//input[@name='mesto']"), 10);
	}
	
	public void setMesto(String mesto){
		WebElement mestoInput = getMesto();
		mestoInput.clear();
		mestoInput.sendKeys(mesto);	
	}
	
	public WebElement getUlica(){
		return Utils.waitForElementPresence(driver, By.xpath("//input[@name='ulica']"), 10);
	}
	
	public void setUlica(String ulica){
		WebElement ulicaInput = getUlica();
		ulicaInput.clear();
		ulicaInput.sendKeys(ulica);	
	}
	
	public WebElement getBroj(){
		return Utils.waitForElementPresence(driver, By.id("broj"), 10);
	}
	
	public void setBroj(String broj){
		WebElement brojInput = getBroj();
		brojInput.clear();
		brojInput.sendKeys(broj);	
	}
	
	public WebElement getBrojStanova(){
		return Utils.waitForElementPresence(driver, By.id("brojStanova"), 10);
	}
	
	public void setBrojStanova(String brojStanova){
		WebElement brojStanovaInput = getBrojStanova();
		brojStanovaInput.clear();
		brojStanovaInput.sendKeys(brojStanova);	
	}
	
	public WebElement getDodajBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@type='submit']"), 10);
	}

	public WebElement getResetBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[@type='button']"), 10);
	}
	
	public void dodajZgradu(String mesto, String ulica, String broj, String brojStanova){
		setMesto(mesto);
		setUlica(ulica);
		setBroj(broj);
		setBrojStanova(brojStanova);
		getDodajBtn().click();
	}

	public WebElement getUspesnoDodataZgradaMsg(){
		return Utils.waitForElementPresence(driver, By.className("toast-success"), 10);
	}
	
	public WebElement getObaveznoPoljeMsg(){
		return Utils.waitForElementPresence(driver, By.className("invalid-feedback"), 10);
	}
	
	public WebElement getZgradaPostojiMsg(){
		return Utils.waitForElementPresence(driver, By.className("toast-error"), 10);
	}
	
	public WebElement getDodavanjeBtn(){
		return Utils.waitForElementPresence(driver, By.className("//button[contains(.,'Dodavanje')]"), 10);
	}
	
	public WebElement getPregledBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'Pregled')]"), 10);
	}
	
	public void navigateToPregledZgrade(){//localhost:8080/zgrade/pregled
		driver.navigate().to("localhost:8080/zgrade/pregled");
	}
//nastavak
	public WebElement getPrikaz(){
		return Utils.waitForElementPresence(driver, By.id("prikaz"), 10);
	}
	
	public String getBrojPrikaza25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();
	}
	
	public WebElement getTabelaZgrada(){
		return Utils.waitForElementPresence(driver, By.tagName("tbody"), 10);
	}
	
	public List<WebElement> getTableRows(){
		return this.getTabelaZgrada().findElements(By.className("table-secondary"));
	}

	public WebElement getZgradaByName(String name) {
		return Utils.waitForElementPresence(driver,(By.xpath("//*[contains(text(),\"" + name + "\")]")), 10);
	}
	
	public WebElement getZgradaPoRednomBroju(String redniBroj) {
		return Utils.waitForElementPresence(driver,(By.xpath("//*[contains(text(),\"" + redniBroj + "\")]")), 10);
	}
	
//	public WebElement getRedniBrojZgrade(){
//		return Utils.waitForElementPresence(driver, By.className("col-md-1"), 10);
//	}
//
//	public String getRedniBroj() {
//		return this.getRedniBrojZgrade().getText();
//	}
	public WebElement getSearchPoljeUlicaBroj() {
		return Utils.waitForElementPresence(driver, By.id("ulicaBroj"), 10);
	}

	public void setSearchPoljeUlicaBroj(String ulicaBroj) {
		WebElement ulicaBrojElement = getSearchPoljeUlicaBroj();
		ulicaBrojElement.clear();
		ulicaBrojElement.sendKeys(ulicaBroj);
	}
	
	public WebElement getSearchPoljeMesto(){
		return Utils.waitForElementPresence(driver, By.id("mesto"), 10);
	}
	
	public void setSearchPoljeMesto(String mesto) {
		WebElement mestoElement = getSearchPoljeMesto();
		mestoElement.clear();
		mestoElement.sendKeys(mesto);
	}
	
	public WebElement getSearchBtn() {
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'Pretraga')]"), 10);
	}
	
	public boolean isTextInTable(String text) {
		return Utils.isPresent(driver, By.xpath("//*[contains(text(),\"" + text + "\")]/../.."));
	}

	
	public WebElement getSecondPageBtn(String n){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,\"" + n + "\")]"), 10);
	}
	
	public WebElement getPreviewPageBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'Izlogujte se')]"), 10);
	}
	
	public WebElement getFirstPageBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'<<')]"), 10);
	}
	
	public WebElement getNextPageBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'>')]"), 10);
	}
	
	public WebElement getLastPageBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'>>')]"), 10);
	}
	
	public WebElement getLogOutBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'>>')]"), 10);
	}
	
	public void getNavigateToStanoviZgrade(Number redniBrojZgrade){
		driver.navigate().to("localhost:8080/zgrada/" + redniBrojZgrade + "/stanovi");
	}
	
	public WebElement getPrikazStanova(){
		return Utils.waitForElementPresence(driver, By.id("prikaz"), 10);
	}
	
	public String getPrikazStanova50(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='50']"), 10).getText();
	}
	
	public WebElement getTabelaStanovi(){
		return Utils.waitForElementPresence(driver, By.tagName("tbody"), 10);
	}

	public List<WebElement> getTabelaRedStanovi(){
		return this.getTabelaZgrada().findElements(By.className("table-secondary"));
	}

	public boolean isStanInTable(String stan){
		return Utils.isPresent(driver, By.xpath("//*[contains(text(),\"" + stan + "\")]/../.."));
	}

	public WebElement getStanByImeVlasnika(String imeVlasnika){
		return Utils.waitForElementPresence(driver, By.xpath("//*[contains(text(),\"" + imeVlasnika + "\")]/../.."), 10);
	}
	
	public WebElement getVlasnikStanariBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//a[contains(.,'Vlasnik i stnari')]"), 10);
	}
	
	public void btnZaVlasnika(String imeVlasnika) {
		this.getStanByImeVlasnika(imeVlasnika).findElement(By.xpath("//a[contains(.,'Vlasnik i stnari')]")).click();
	}
	
	public WebElement getNaslovAdresaZgrade(){
		return Utils.waitForElementPresence(driver, By.cssSelector("h5.ng-star-inserted"), 10);
	}
	
	public WebElement getPrikazKorisnici(){
		return Utils.waitForElementPresence(driver, By.id("prikaz"), 10);
	}
	
	public String getPrikazKorisnici25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();
	}
	
	public WebElement getInputPolje(){
		return Utils.waitForElementPresence(driver, By.xpath("//input[@placeholder='Unesite ime, prezime ili email']"), 10);
	}
	
	public WebElement getFiltrirajBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//a[contains(.,'Filtriraj')]"), 10);
	}
	
	public WebElement getTabelaKorisnika(){
		return Utils.waitForElementPresence(driver, By.id("korisnici"), 10);
	}
	
	public List<WebElement> getListaKorisnikaRed(String red){
		return this.getTabelaKorisnika().findElements(By.cssSelector("#korisnici > tbody > tr:nth-child(" + red + ")"));
	}
	
	public WebElement getPostaviVlasnikaBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id=\"stanari\"]/tbody/tr/td[2]/div[2]/button"), 20);
	}

	
	public String getUspesnoDodatVlasnikMsg(){
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='toast-container']/div/div"), 10).getText();
	}
	
	public String getNaslovVlasnik(){
		return Utils.waitForElementPresence(driver, By.className("col-md-10"), 15).getText();
	}
	
	public WebElement getUkloniVlasnikaBtn(){
		return Utils.waitForElementPresence(driver, By.id("ukloniVlasnika"), 15);
	}
	
	public String getUspesnoUklonjenVlasnikMsg(){
		return Utils.waitForElementPresence(driver, By.className("toast-success"), 10).getText();
	}
	
	public WebElement getDodajStanareBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//button[contains(.,'Dodaj u stanare')]"), 10);
	}
	
	public String getUspesnoDodatStanarMsg(){
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id='toast-container']/div"), 15).getText();
	}
	
	public WebElement getUkloniStanaraBtn(){
		return Utils.waitForElementPresence(driver, By.cssSelector("#stanari > tbody > tr > td.col-md-6 > div:nth-child(3) > button"), 15);
	}
	
	public String getUspesnoUklonjenStanarMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@aria-label='Uspesno ste uklonili stanara!']"), 15).getText();
	}	
	
	public WebElement getTabelaStanara(){
		return Utils.waitForElementPresence(driver, By.id("stanari"), 15);
	}
	
	public WebElement getPostaviPredsednikaBtn(){
		return Utils.waitForElementPresence(driver, By.xpath("//*[@id=\"stanari\"]/tbody/tr/td[2]/div[1]/button"), 15);
	}
	
	public String getUspesnoDodatPredsednikMsg() {
		return Utils.waitForElementPresence(driver, By.xpath("//*[@aria-label='Uspesno ste postavili predsednika zgrade!']"), 15).getText();
	}
	
	public WebElement getObavestenje(){
		return Utils.waitToBeClickable(driver, By.xpath("//a[@href=('/zgrada/1/obavestenja')]"), 15);
	}
	
	public void getNavigateToObavestenja(){
		driver.navigate().to("localhost:8080/zgrada/1/obavestenja");
	}
	
	public WebElement getPrikazObavestenja(){
		return Utils.waitForElementPresence(driver, By.className("form-control"), 15);
	}
	
	public String getPrikazObavestenja25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();
	}
	
	public WebElement getPredloziTackeDnevnogReda(){
		return Utils.waitToBeClickable(driver, By.xpath("//a[@href='/zgrada/1/tacke']"), 15);
	}
	
	public void getNavigateToPredloziTackeDnevnogReda(){
		driver.navigate().to("localhost:8080/zgrada/1/tacke");
	}
	
	public WebElement getPrikazPredloziTackeDnevnogReda(){
		return Utils.waitForElementPresence(driver, By.className("form-control"), 15);
	}
	
	public String getPrikazPredloziTackeDnevnogReda25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();
	}
	
	public WebElement getSastanciSkupstine(){
		return Utils.waitToBeClickable(driver, By.xpath("//a[@href='/zgrada/1/sastanci']"), 15);
	}
	
	public void getNavigateToSastanciSkupstine(){
		driver.navigate().to("localhost:8080/zgrada/1/sastanci");
	}
	
	public WebElement getPrikazSastanciSkupstine(){
		return Utils.waitForElementPresence(driver, By.className("form-control"), 15);
	}
	
	public String getPrikazSastanciSkupstine25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();
	}
	
	public WebElement getFilter(){
		return Utils.waitForElementPresence(driver, By.className("custom-select"), 15);
	}
	
	public String getFilterSastanciUToku(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='Sastanci u toku']"), 10).getText();
	}

	public WebElement getPregledajTacke() {
		return Utils.waitForElementPresence(driver, By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-sastanci/div/div/table/tbody/tr[5]/td/span[1]/a/span"), 10);
	}
	
	
	public String getFilterZavrseniSastanci(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='Prosli Sastanci']"), 10).getText();
	}
	
	
	public WebElement getZapisnik(){
		return Utils.waitForElementPresence(driver, By.xpath("//span[contains(., 'Pregledaj zapisnik')]"), 10);
	}
	
	public WebElement getAnketa() {
		return Utils.waitForElementPresence(driver, By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-tacke-sastanci/div[1]/div/table/tbody/tr[4]/td/span/a"), 10);
	}
	public WebElement getKvarovi(){
		return Utils.waitToBeClickable(driver, By.xpath("//a[@href='/zgrada/1/kvarovi']"), 15);
	}
	
	public void getNavigateToKvarovi(){
		driver.navigate().to("localhost:8080/zgrada/1/kvarovi");
	}
	
	public WebElement getPrikazKvarovi(){
		return Utils.waitForElementPresence(driver, By.className("form-control"), 15);
	}
	
	public String getPrikazKvarovi25(){
		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='25']"), 10).getText();
	}
	
	public WebElement getCheckbox(){
		return Utils.waitForElementPresence(driver, By.xpath("//input[@type='checkbox']"), 15);
	}
	
//	public String getFilterSastanciUtoku(){
//		return Utils.waitForElementPresence(driver, By.xpath("//option[@value='Sastanci u toku']"), 10).getText();
//	}
	
}
	
