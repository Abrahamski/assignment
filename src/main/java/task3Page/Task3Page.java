package task3Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Task3Page {

private WebDriver driver;
	
	public Task3Page (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getLoginBtn() {
		return driver.findElement(By.xpath("//*[@id='btnLogin']"));
	}
	
	public void clickBtn() {
		WebElement btn = this.getLoginBtn();
		btn.click();
	}
	
	public WebElement getTitle() {
		return driver.findElement(By.className("page-title"));
	}
	
	public WebElement getRecruitment() {
		return driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[2]/div[1]/div/div[5]/ul/li[7]/a/span[2]"));
	}
	
	public void clickRecruitment() {
		WebElement recruitment = this.getRecruitment();
		recruitment.click();
	}
	
	public WebElement getCandidate() {
		return driver.findElement(By.xpath("//*[@id='menu_recruitment_viewCandidates']"));
	}
	
	public void clickCandidate() {
		WebElement candidate = this.getCandidate();
		candidate.click();
	}
	
	public WebElement getNumberOfCandidates() {
		return driver.findElement(By.cssSelector("#fromToOf > div:nth-child(1)"));
	}
	
	public WebElement getAddBtn() {
		return driver.findElement(By.id("addItemBtn"));
	}
	
	public void clickOnAddBtn() {
		WebElement addButton = this.getAddBtn();
		addButton.click();
	}
	
	public WebElement getCustomHeader() {
		return driver.findElement(By.xpath("//*[@id='modalAddCandidate']/div[1]"));
	}
}
