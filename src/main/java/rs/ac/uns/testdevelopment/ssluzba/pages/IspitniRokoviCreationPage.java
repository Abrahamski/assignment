package rs.ac.uns.testdevelopment.ssluzba.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IspitniRokoviCreationPage {
	private WebDriver driver;

	public IspitniRokoviCreationPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getModalDialog(){
		return driver.findElement(By.className("modal-dialog"));
	}
	
	public WebElement getModalTitle() {
		return driver.findElement(By.id("myIspitniRokoviLabel"));
	}
	public WebElement getNaziv() {
		return driver.findElement(By.name("naziv"));
	}
	
	public void setNaziv(String value){
		WebElement el = this.getNaziv();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getPocetak() {
		return driver.findElement(By.id("field_pocetak"));
	}
	
	public void setPocetak(String value){
		WebElement el = this.getPocetak();
		el.clear();
		el.sendKeys(value);
	}
	
	public WebElement getKraj() {
		return driver.findElement(By.id("field_kraj"));
	}
	
	public void setKraj(String value){
		WebElement el = this.getKraj();
		el.clear();
		el.sendKeys(value);
	}
	
	
	public WebElement getCancelBtn(){
		return getModalDialog().findElement(By.className("btn-default"));
	}
	
	public WebElement getSaveBtn(){
		return getModalDialog().findElement(By.className("btn-primary"));
	}
	
	public void createIspitniRok(String naziv, String pocetak, String kraj) {
		setNaziv(naziv);
		setPocetak(pocetak);
		setKraj(kraj);
		getSaveBtn().click();
	}
}
