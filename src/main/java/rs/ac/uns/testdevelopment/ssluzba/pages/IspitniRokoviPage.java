package rs.ac.uns.testdevelopment.ssluzba.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IspitniRokoviPage {
	private WebDriver driver;

	public IspitniRokoviPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebElement getCreateBtn(){
		WebElement btn = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button [@ui-sref=\"ispitniRokovi.new\"]")));
		return btn;
	}

	public WebElement getIspitniRokoviTable() {
		return driver.findElement(By.className("jh-table"));
	}

	public List<WebElement> getTableRows() {
		return this.getIspitniRokoviTable().findElements(By.tagName("tr"));
	}

	public WebElement getIspitniRokByName(String index) {
		return driver.findElement(By.xpath("//*[contains(text(),\"" + index + "\")]/../.."));
	}

	public void deleteIspitniRokByName(String index) {
		getIspitniRokByName(index).findElement(By.className("btn-danger")).click();
	}

	public void editIspitniRokByName(String index) {
		getIspitniRokByName(index).findElement(By.className("btn-primary")).click();
	}

	public void viewIspitniRokByName(String index) {
		getIspitniRokByName(index).findElement(By.className("btn-info")).click();
	}

}
