package rs.ac.uns.testdevelopment.ssluzba.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MenuPage {
	private WebDriver driver;

	public MenuPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebElement getAccountMenu() {
		return driver.findElement(By.id("account-menu"));
	}

	public WebElement getSignUp() {
		WebElement signUp = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a [@ui-sref=\"login\"]")));
		return signUp;
	}

	public WebElement getEntities() {
		return driver.findElement(By.linkText("Entities"));
	}

	public WebElement getStudentsLink() {
		return driver.findElement(By.xpath("//a [@ui-sref=\"studenti\"]"));
	}
	
	public WebElement getIspitniRokoviLink() {
		return driver.findElement(By.xpath("//a [@ui-sref=\"ispitniRokovi\"]"));
	}

	public WebElement getLogOut() {
		return driver.findElement(By.id("logout"));
	}

}