package task2Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import junit.framework.Assert;
import org.openqa.selenium.remote.DesiredCapabilities;
import task2Page.Task2Page;

public class Task2Test {
	
	private WebDriver driver;
	private Task2Page task2Page;
	private String baseUrl;
	
	@BeforeClass
	public void setupSelenium() {
		
		System.setProperty("webdriver.gecko.driver", "geckodriver");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		WebDriver driver = new FirefoxDriver(capabilities);
		baseUrl = "https://www.google.com/";
		driver.navigate().to(baseUrl);
		
		task2Page = new Task2Page(driver);
		
	}
	
	@Test(priority = 0)
	public void searchOption() throws InterruptedException {
		task2Page.setSearch("cheese");
		Thread.sleep(5000);
		String result = task2Page.resultNumber().getText().split(" ")[2];
		int iResult = Integer.parseInt(result);
		
		System.out.println(iResult);
		//assertFalse(number.equals(777));
	
	}
}