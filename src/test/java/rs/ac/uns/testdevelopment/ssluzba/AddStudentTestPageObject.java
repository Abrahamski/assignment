package rs.ac.uns.testdevelopment.ssluzba;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import rs.ac.uns.testdevelopment.ssluzba.pages.LoginPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.MenuPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.ModalDeletePage;
import rs.ac.uns.testdevelopment.ssluzba.pages.StudentsCreationPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.StudentsListPage;

@Test
public class AddStudentTestPageObject {
	private WebDriver driver;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private StudentsListPage studentsListPage;
	private StudentsCreationPage studentsCreationPage;
	private ModalDeletePage modalDeletePage;
	
	@BeforeMethod
	public void setupSelenium() {
		System.setProperty("webdriver.gecko.driver", "geckodriver");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		driver.navigate().to("http://www.google.com/");
		
		//init pages
		/*loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		studentsListPage = new StudentsListPage(driver);
		studentsCreationPage = new StudentsCreationPage(driver);
		modalDeletePage = new ModalDeletePage(driver);*/
	}
	
	/*public void studentCreation() throws InterruptedException {
		menuPage.getAccountMenu().click();
		assertEquals(true, menuPage.getSignUp().isDisplayed());
		menuPage.getSignUp().click();
		
		loginPage.login("admin", "admin");
		
		menuPage.getEntities().click();
		assertTrue(menuPage.getStudentsLink().isDisplayed());
		menuPage.getStudentsLink().click();
		
		Thread.sleep(1000);
		assertTrue(studentsListPage.getStudentsTable().isDisplayed());
		assertEquals(1, studentsListPage.getTableRows().size());
		WebElement createBtn = studentsListPage.getCreateBtn();
		assertTrue(createBtn.isDisplayed());
		createBtn.click();
		//verify is modal present
		assertTrue(studentsCreationPage.getModalDialog().isDisplayed());
		assertEquals("Create or edit a Studenti", studentsCreationPage.getModalTitle().getText());
		//check visibility
		assertTrue(studentsCreationPage.getIndex().isDisplayed());
		assertTrue(studentsCreationPage.getIme().isDisplayed());
		assertTrue(studentsCreationPage.getPrezime().isDisplayed());
		assertTrue(studentsCreationPage.getGrad().isDisplayed());
		assertTrue(studentsCreationPage.getCancelBtn().isDisplayed());
		assertTrue(studentsCreationPage.getSaveBtn().isDisplayed());
		
		studentsCreationPage.setIndex("RA 43-2011");
		studentsCreationPage.setIme("Miroslav");
		studentsCreationPage.setPrezime("Kondic");
		studentsCreationPage.setGrad("Novi Sad");
		studentsCreationPage.getSaveBtn().click();
		//
		Thread.sleep(2000);

		//check row data
		WebElement studentRow = studentsListPage.getStudentRowByIndex("RA 43-2011");
		String rowData = studentRow.getText();
		assertTrue(rowData.contains("RA 43-2011 Miroslav Kondic"));
		//add new student via helper function
		createBtn = studentsListPage.getCreateBtn();
		assertTrue(createBtn.isDisplayed());
		createBtn.click();
		studentsCreationPage.createStudent("RA 45-2011", "Novi", "Korisnik", "Novi Sad");
		studentRow = studentsListPage.getStudentRowByIndex("RA 45-2011");
		rowData = studentRow.getText();
		assertTrue(rowData.contains("RA 45-2011 Novi Korisnik"));
		
		//check table count
		assertEquals(3, studentsListPage.getTableRows().size());
		
		studentsListPage.deleteStudentByIndex("RA 43-2011");		
		WebElement modalDelete = modalDeletePage.getModal();
		assertTrue(modalDelete.isDisplayed());
		modalDeletePage.confirmDelete();
		
		Thread.sleep(2000);
		studentsListPage.deleteStudentByIndex("RA 45-2011");
		modalDeletePage.confirmDelete();
		Thread.sleep(2000);
		assertEquals(1, studentsListPage.getTableRows().size());
	}*/
	/*
	@AfterMethod
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
	}*/
	
	
}

