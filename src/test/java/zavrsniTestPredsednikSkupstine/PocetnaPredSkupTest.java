package zavrsniTestPredsednikSkupstine;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import zavrsniPredsednikSkupstine.LoginPredSkupPage;
import zavrsniPredsednikSkupstine.PocetnaPredSkupPage;
import zavrsniPredsednikSkupstine.PromenaLozinkePage;

public class PocetnaPredSkupTest {

	
	private WebDriver driver;
	private String baseUrl;
	private LoginPredSkupPage loginPredSkupPage;
	private PromenaLozinkePage promenaLozinkePage;
	private PocetnaPredSkupPage pocetnaPredSkupPage;

	@BeforeClass
	public void setupSelenium() {

		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080";
		driver.navigate().to(baseUrl);

		loginPredSkupPage = new LoginPredSkupPage(driver);
		pocetnaPredSkupPage = new PocetnaPredSkupPage(driver);
		loginPredSkupPage.login("predSkup@gmail.com", "Bar5slova");
	}
	
	//dodavanje skupstine
	@Test(priority = 0)
	public void testZakaziSkupstinu() throws InterruptedException {

		pocetnaPredSkupPage.getStranicaZgradaUKojimaJeVlasnik();
		pocetnaPredSkupPage.navigateToPageZgradaUKojojJeVlasnik();
		pocetnaPredSkupPage.getSastanciSkupstine().click();
		pocetnaPredSkupPage.getDodajSastanakBtn().click();
		pocetnaPredSkupPage.setPocetakSkupstine("08252021", "0800PM");
		pocetnaPredSkupPage.setZavrsetakSkupstine("08252021", "0930PM");
		pocetnaPredSkupPage.getPotvrdiSastanakBtn().click();
		
		String uspesnoKreiranaSkupstina = "Sastanak uspesno zakazan";
		assertEquals(uspesnoKreiranaSkupstina, pocetnaPredSkupPage.getSastanakUspesnoZakazanMsg());
	
	}
	
	//dodavanje PTDR buducoj skupstini
	@Test(priority = 1)
	public void testDodajPDTRSkupstini() throws InterruptedException {

		pocetnaPredSkupPage.navigateToPagePocetnaPredSkup();
		pocetnaPredSkupPage.getStranicaZgradaUKojimaJeVlasnik();
		pocetnaPredSkupPage.navigateToPageZgradaUKojojJeVlasnik();
		pocetnaPredSkupPage.getPredloziTackeDnevnogReda().click();
		//pocetnaPredSkupPage.getBududcuSkupstinu();
		Select dropdown = new Select(pocetnaPredSkupPage.getSelektPonudjenihSkupstina());
		dropdown.selectByValue("1: Object");
		pocetnaPredSkupPage.getDodajTackuBtn().click();
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='toast-container']/div/div")));
		
		String uspesnoDodeljenPTDRSkupstini = "Tacka uspesno dodata u skupstinu";
		assertEquals(uspesnoDodeljenPTDRSkupstini, pocetnaPredSkupPage.getUspesnoDodeljenaTackaMsg());
	
	}
	
	@Test(priority = 2)
	public void testDodavanjeKvara() {
		
		pocetnaPredSkupPage.navigateToPagePocetnaPredSkup();
		pocetnaPredSkupPage.getStranicaZgradaUKojimaJeVlasnik();
		pocetnaPredSkupPage.navigateToPageZgradaUKojojJeVlasnik();
		pocetnaPredSkupPage.getKvarovi().click();
		pocetnaPredSkupPage.getDodajKvarBtn().click();
		pocetnaPredSkupPage.setKvar("Novi Sad", "Kvar broj 1", "Marko Markovic");
		pocetnaPredSkupPage.getSacuvajKvarBtn().click();
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='toast-container']/div/div")));
		
		String uspesnoDodatKvar = "Kvar uspesno dodat";
		assertEquals(uspesnoDodatKvar, pocetnaPredSkupPage.getUspesnoSacuvanKvarMsg());
	}
	
	@Test(priority = 3)
	public void testGlasanjeAnketa() {
		
		pocetnaPredSkupPage.navigateToPagePocetnaPredSkup();
		pocetnaPredSkupPage.getStranicaZgradaUKojimaJeVlasnik();
		pocetnaPredSkupPage.getSastanciSkupstine().click();
		pocetnaPredSkupPage.setPregledTackeBtn().click();
		pocetnaPredSkupPage.setPogledajAnketu("Predlazem da se krece zidovu u hodniku");
		WebElement radioZ = driver.findElement(By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-izlistaj-ankete/div/app-pitanje/div/fieldset/div/div[1]/div[1]/div/div/label"));
		WebElement radioB = driver.findElement(By.xpath("/html/body/app-root/app-zgrada/div/div[2]/app-izlistaj-ankete/div/app-pitanje/div/fieldset/div/div[2]/div[1]/div/div/label"));
		radioZ.click();
		
		
//		WebDriverWait wait = new WebDriverWait(driver,30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='toast-container']/div/div")));
//		
//		String uspesnoDodatKvar = "Kvar uspesno dodat";
//		assertEquals(uspesnoDodatKvar, pocetnaPredSkupPage.getUspesnoSacuvanKvarMsg());
	}
	
	
}
