package zavrsniTestPredsednikSkupstine;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniPredsednikSkupstine.LoginPredSkupPage;
import zavrsniPredsednikSkupstine.PromenaLozinkePage;

public class PromenaLozinkeTest {
	
	private WebDriver driver;
	private String baseUrl;
	private LoginPredSkupPage loginPredSkupPage;
	private PromenaLozinkePage promenaLozinkePage;

	@BeforeClass
	public void setupSelenium() {

		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080";
		driver.navigate().to(baseUrl);

		loginPredSkupPage = new LoginPredSkupPage(driver);
		promenaLozinkePage = new PromenaLozinkePage(driver);
		
		//promenaLozinkePage.navigateToPromenaLozinke();
	
	}
	
	@Test(priority = 0)
	public void negativanTestNepostojecaLozinka() throws InterruptedException{
		
		loginPredSkupPage.login("predSkup@gmail.com", "Bar5slova");

		promenaLozinkePage.getPromenaLozinke().click();
		promenaLozinkePage.getFormaZaPromenuLozinke().isDisplayed();
//	}
//	
//	@Test(priority = 1)
//	public void negativanTestNepostojecaLozinka() throws InterruptedException{
		//nepostojeca lozinka
		//loginPredSkupPage.login("predSkup@gmail.com", "Bar5slova");
		
		promenaLozinkePage.setPromeniLozinku("Bar66slova", "Bar7slova", "Bar7slova");
		String pogresnaLozinka = promenaLozinkePage.getPogresnaLozinkaMsg();
		assertEquals(pogresnaLozinka, "Pogresna lozinka!");
	}
	
	@Test(priority = 2)
	public void negativanTestNeispravanFormatLozinke() throws InterruptedException{
		
		//neispravan format nove lozinke
		promenaLozinkePage.setPromeniLozinku("Bar5slova", "Barslova", "Barslova");
		String neispravanFormatLozinke = promenaLozinkePage.getNeispravanFormatLozinkeMsg();
		assertEquals(neispravanFormatLozinke, "Neispravna lozinka! Pogledajte napomenu.");
	
	}
	
	@Test(priority = 3)
	public void negativanTestNepoklapanjeLozinki() throws InterruptedException{
	
		//Ne poklapanje lozinki
		promenaLozinkePage.setPromeniLozinku("Bar5slova", "Bar6slova", "Bar7slova");
		String nepokalapanjeLozinki = promenaLozinkePage.getNepoklapanjeLozinkiMsg();
		assertEquals(nepokalapanjeLozinki, "Lozinke se ne poklapaju!");
		
		loginPredSkupPage.getIzlogujSeBtn().click();
		
	}
	
	@Test(priority = 4)
	public void pozitivanTestPromenaLozinke() throws InterruptedException{
		
		loginPredSkupPage.login("predSkup@gmail.com", "Bar5slova");

		promenaLozinkePage.getPromenaLozinke().click();
		promenaLozinkePage.getFormaZaPromenuLozinke().isDisplayed();
		
		promenaLozinkePage.setPromeniLozinku("Bar5slova", "Bar6slova", "Bar6slova");
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='toast-container']/div/div")));
	//	promenaLozinkePage.navigateToPocetna();
		String uspesnoPromenjenaLozinka = promenaLozinkePage.getUspesnoPromenjenaLozinkaMsg();
		assertEquals(uspesnoPromenjenaLozinka, "Lozinka uspesno izmenjena!");
		//String ocekivaniUrl = baseUrl;
		//Assert.assertEquals(ocekivaniUrl, driver.getCurrentUrl());
		
	}
}
