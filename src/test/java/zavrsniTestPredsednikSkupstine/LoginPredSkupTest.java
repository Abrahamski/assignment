package zavrsniTestPredsednikSkupstine;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniPredsednikSkupstine.LoginPredSkupPage;

public class LoginPredSkupTest {

	private WebDriver driver;
	private LoginPredSkupPage loginPredSkupPage;
	private String baseUrl;
	
	@BeforeClass
	public void setupSelenium() {

		//System.setProperty("webdriver.gecko.driver", "geckodriver");
		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080/logovanje";
		driver.navigate().to(baseUrl);
		
		loginPredSkupPage = new LoginPredSkupPage(driver);
		//registracijaPage = new RegistracijaPage(browser);
	}

	@Test(priority = 0)
	public void login() throws InterruptedException {

		loginPredSkupPage.login("predSkup@gmail.com", "Bar5slova");

		WebElement potvrdaLogina = loginPredSkupPage.getEmailFromMenu();
		String emailZaglavlje = potvrdaLogina.getText();
		assertTrue(emailZaglavlje.contains("predSkup@gmail.com"));
		//assertEquals("admin@gmail.com", emailZaglavlje);
	}
	
	@Test(priority = 1)
	public void logout() throws InterruptedException {

		loginPredSkupPage.logOut();
		
		String url = baseUrl;
		assertTrue(true, url);
	}
	
	@Test(priority = 2)
	public void loginNeispravnFormatLozinke() throws InterruptedException {
		
		loginPredSkupPage.login("predSkup@gmail.com", "2");
		String porukaLozinka = "Lozinka nije validnog formata (Mora biti bar jedno veliko slovo, veliko malo slovo i broj i minimalne duzine 6)!";
		assertEquals(porukaLozinka, loginPredSkupPage.getNeispravnaLozinkaLoginMsg());
	}
	
	@Test(priority = 3)
	public void loginNeispravanEmailFormat() throws InterruptedException {		
		
		loginPredSkupPage.login("fdsaf@", "Bar5slova");
		String porukaEmailForm = "Email nije validnog formata!";
		assertEquals(porukaEmailForm, loginPredSkupPage.getNeispravanEmailLoginMsg());
	}
	
	@Test(priority = 4)
	public void loginNeispravanEmailUnos() throws InterruptedException {	
		
		loginPredSkupPage.login("predS@gmail.com", "Bar5slova");
		String porukaEmail = "Pogresan email ili lozinka!";
		assertEquals(porukaEmail, loginPredSkupPage.getNeispravanEmailLoginMsg());
	}
	
	@Test(priority = 5)
	public void loginNeispravnaLozinka() throws InterruptedException {
		
		loginPredSkupPage.login("predSkup@gmail.com", "Bar6slova");
		String porukaLozinka = "Pogresan email ili lozinka!";
		assertEquals(porukaLozinka, loginPredSkupPage.getNeispravnaLozinkaLoginMsg());
	}
	
	@AfterClass
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
	}

}
