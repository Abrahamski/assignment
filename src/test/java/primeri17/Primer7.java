package primeri17;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;

public class Primer7 {
	public static void main(String[] args) throws InterruptedException {
		// napomena, ne radi trenutno na Firefox-u
		// pokusati na Chrome
//		System.setProperty("webdriver.chrome.driver", "chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().to("http://demos.devexpress.com/aspxeditorsdemos/ListEditors/MultiSelect.aspx");
		driver.manage().window().maximize();
		
		driver.findElement(By.id("ControlOptionsTopHolder_lbSelectionMode_B-1")).click();
		driver.findElement(By.id("ControlOptionsTopHolder_lbSelectionMode_DDD_L_LBI1T0")).click();
		Thread.sleep(5000);
		
		// Perform Multiple Select
		Actions builder = new Actions(driver);
		WebElement select = driver.findElement(By.id("ContentHolder_lbFeatures_LBT"));
		List<WebElement> options = select.findElements(By.tagName("td"));
		System.out.println("Broj selektovanih elemenata je" + options.size());
		Action multipleSelect = builder.keyDown(Keys.CONTROL).click(options.get(2)).click(options.get(4))
				.click(options.get(6)).build();
		multipleSelect.perform();
		Thread.sleep(5000);
		driver.close();
	}
}
