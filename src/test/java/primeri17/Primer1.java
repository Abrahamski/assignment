package primeri17;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

/*
 * Primer 1 
 * Java kod primera CalculatorTest dobijen
 * pomocu SeleniumIDE plugin-a
 * 
 * Klasa Primer1 sadrzi main metodu tako da moze zasebno da se pokrene
 */
public class Primer1 {
	public static void main(String[] args) {
		// Instanciramo pretraživač
		// u ovom slučaju to je Firefox
		// Od verzije 3.0.0 Selenium webDriver zahteva putanju do
		// Mozilla Firefox geckodrivera
		// 2.53 vezija ne radi sa novijim verzijama Firefox-a
		
		
		System.setProperty("webdriver.gecko.driver", "geckodriver");
		WebDriver driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().to("http://www.calculator.net/");
		driver.manage().window().maximize();		
		driver.findElement(By.xpath("//*[@id=\"hl3\"]/li[3]/a")).click();		
		driver.findElement(By.id("cpar1")).sendKeys("10");
		driver.findElement(By.id("cpar2")).sendKeys("50");
		driver.findElement(By.xpath(".//*[@id='content']/table[1]/tbody/tr[2]/td/input[2]")).click();
		String result = driver.findElement(By.cssSelector("#content > p.verybigtext > font > b")).getText();
		System.out.println(" The Result is " + result);
		driver.quit();
	}
}