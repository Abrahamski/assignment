package primeri18;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CalculatorFractionPage {
	private WebDriver driver;

	public CalculatorFractionPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getT1() {
		return this.driver.findElement(By.id("t1"));
	}
	
	public void setT1(String value) {
		WebElement t1 = this.getT1();
		t1.clear();
		t1.sendKeys(value);
	}
	
	public WebElement getT2() {
		return this.driver.findElement(By.id("t2"));
	}
	
	public void setT2(String value) {
		WebElement t2 = this.getT2();
		t2.clear();
		t2.sendKeys(value);
	}
	
	public WebElement getB1() {
		return this.driver.findElement(By.id("b1"));
	}
	
	public void setB1(String value) {
		WebElement b1 = this.getB1();
		b1.clear();
		b1.sendKeys(value);
	}
	public WebElement getB2() {
		return this.driver.findElement(By.id("b2"));
	}
	
	public void setB2(String value) {
		WebElement b1 = this.getB2();
		b1.clear();
		b1.sendKeys(value);
	}
	
	public Select getOperation(){
		WebElement el = driver.findElement(By.id("op"));
		Select dropdown = new Select(el);
		return dropdown;
	}
	
	public void setOperation(String op) {
		Select opertion = this.getOperation();
		opertion.selectByValue(op);
	}
	
	public WebElement getCalculateButton(){
		return driver.findElement(By.xpath(".//*[@id=\"content\"]/table[1]/tbody/tr[4]/td/input[2]"));
	}
	
	public void calculate(String t1, String t2, String b1, String b2, String operation){
		setT1(t1);
		setT2(t2);
		setB1(b1);
		setB2(b2);
		setOperation(operation);
		getCalculateButton().click();
	}

}