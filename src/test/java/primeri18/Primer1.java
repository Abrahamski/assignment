package primeri18;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Primer1 {
	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "geckodriver");
		WebDriver driver = new FirefoxDriver();
		CalculatorPercentagePage percentagePage = new CalculatorPercentagePage(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().to("http://www.calculator.net/percent-calculator.html");
		driver.manage().window().maximize();	
		
//		percentagePage.setFirstNumber("10");
//		percentagePage.setSecondNumber("50");
//		percentagePage.getCalculateButton().click();
		percentagePage.calculate("10", "50");

		String result = percentagePage.getResultText();
		System.out.println(" The Result is " + result);
		driver.close();
	}
}