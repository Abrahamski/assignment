package primeri18;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PrimerFraction {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "geckodriver");
		WebDriver driver = new FirefoxDriver();
		
		CalculatorFractionPage fractionPage = new CalculatorFractionPage(driver);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.navigate().to("http://www.calculator.net/fraction-calculator.html");
		driver.manage().window().maximize();
		
		fractionPage.setT1("1");
		fractionPage.setB1("10");
		
		fractionPage.setT2("3");
		fractionPage.setB2("10");
		
		fractionPage.setOperation("*");
		
		fractionPage.getCalculateButton().click();
		
//		System.out.println(" The Result is " + result);
		driver.close();

	}

}
