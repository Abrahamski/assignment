package zavrsniTestAdmin;

import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniAdmin.LoginAdminPage;
import zavrsniAdmin.PocetnaPage;

public class PocetnaPageTest {
	
	private WebDriver driver;
	private PocetnaPage pocetnaPage;
	private LoginAdminPage loginAdminPage;
	private String baseUrl;
	
	@BeforeClass
	public void setupSelenium() {

		//System.setProperty("webdriver.gecko.driver", "geckodriver");
		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080";
		driver.navigate().to(baseUrl);
		
		loginAdminPage = new LoginAdminPage(driver);
		pocetnaPage = new PocetnaPage(driver);
		//registracijaPage = new RegistracijaPage(browser);
		loginAdminPage.login("admin@gmail.com", "Bar5slova");
	}
	
	@Test(priority = 0)
	public void toZgradePage() throws InterruptedException {

		//TESTIRANJE KADA SE KLIKNE NA ZGRADE IZ MENI LINIJE
		//pocetnaPage.getZgradeMeni().click();
		//pocetnaPage.navigateToPageZgrade();
		
		//TESTIRANJE KADA SE KLIKNE NA ZGRADE U OPCIJAMA NA POCETNOJ STRANI
		pocetnaPage.getZgradeOpcije().click();
		pocetnaPage.navigateToPageZgrade();
		
		String urlZgrade = baseUrl + "/zgrade";
		assertTrue(true, urlZgrade);
	}

	@Test(priority = 1)
	public void toStanariPage() throws InterruptedException {

		//TESTIRANJE KADA SE KLIKNE NA STANARE IZ MENI LINIJE
		//pocetnaPage.getStanariMeni().click();
		//pocetnaPage.navigateToPageStanari();
		
		//TESTIRANJE KADA SE KLIKNE NA STANARE U OPCIJAMA NA POCETNOJ STRANI
		pocetnaPage.getPocetnaMeni().click();
		pocetnaPage.navigateToPagePocetna();
		pocetnaPage.getStanariOpcije().click();
		pocetnaPage.navigateToPageStanari();
		
		String urlStanari = baseUrl + "/stanari";
		assertTrue(true, urlStanari);
	}
	
//	@Test(priority = 2)
//	public void toInstitucijePage() throws InterruptedException {
//
//		pocetnaPage.getInstitucijeMeni().click();
//		//pocetnaPage.getInstitucijeOpcije().click();
//		pocetnaPage.navigateToPageInstitucije();
//		
//		String urlInstitucije = baseUrl + "/";
//		assertTrue(true, urlInstitucije);
//	}
//
//	@Test(priority = 3)
//	public void toFirmePage() throws InterruptedException {
//
//		pocetnaPage.getFirmeMeni().click();
//		//pocetnaPage.getFirmeOpcije().click();
//		pocetnaPage.navigateToPageZgrade();
//		
//		String urlStanari = baseUrl + "/";
//		assertTrue(true, urlStanari);
//	}
	
	//@AfterTest
	//public void logout() {
		//pocetnaPage.logOut();
	//}
	
	
	@AfterClass
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
	}
}
