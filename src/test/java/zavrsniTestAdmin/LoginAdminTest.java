package zavrsniTestAdmin;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniAdmin.LoginAdminPage;

public class LoginAdminTest {
	
	private WebDriver driver;
	private LoginAdminPage loginAdminPage;
	private String baseUrl;
	
	@BeforeClass
	public void setupSelenium() {

		//System.setProperty("webdriver.gecko.driver", "geckodriver");
		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080/logovanje";
		driver.navigate().to(baseUrl);
		
		loginAdminPage = new LoginAdminPage(driver);
		//registracijaPage = new RegistracijaPage(browser);
	}

	@Test(priority = 0)
	public void login() throws InterruptedException {

		loginAdminPage.login("admin@gmail.com", "Bar5slova");

		WebElement potvrdaLogina = loginAdminPage.getEmailFromMenu();
		String emailZaglavlje = potvrdaLogina.getText();
		assertTrue(emailZaglavlje.contains("admin@gmail.com"));
		//assertEquals("admin@gmail.com", emailZaglavlje);
	}
	
	@Test(priority = 1)
	public void logout() throws InterruptedException {

		loginAdminPage.logOut();
		
		String url = baseUrl;
		assertTrue(true, url);
	}
	
	@Test(priority = 2)
	public void loginNeispravnFormatLozinke() throws InterruptedException {
		
		loginAdminPage.login("admin@gmail.com", "2");
		String porukaLozinka = "Lozinka nije validnog formata (Mora biti bar jedno veliko slovo, veliko malo slovo i broj i minimalne duzine 6)!";
		assertEquals(porukaLozinka, loginAdminPage.getNeispravnaLozinkaLoginMsg());
	}
	
	@Test(priority = 3)
	public void loginNeispravanEmailFormat() throws InterruptedException {		
		
		loginAdminPage.login("fdsaf@", "Bar5slova");
		String porukaEmailForm = "Email nije validnog formata!";
		assertEquals(porukaEmailForm, loginAdminPage.getNeispravanEmailLoginMsg());
	}
	
	@Test(priority = 4)
	public void loginNeispravanEmailUnos() throws InterruptedException {	
		
		loginAdminPage.login("amin@gmail.com", "Bar5slova");
		String porukaEmail = "Pogresan email ili lozinka!";
		assertEquals(porukaEmail, loginAdminPage.getNeispravanEmailLoginMsg());
	}
	
	@Test(priority = 5)
	public void loginNeispravnaLozinka() throws InterruptedException {
		
		loginAdminPage.login("admin@gmail.com", "Bar6slova");
		String porukaLozinka = "Pogresan email ili lozinka!";
		assertEquals(porukaLozinka, loginAdminPage.getNeispravnaLozinkaLoginMsg());
	}
	
	@AfterClass
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
	}

}
