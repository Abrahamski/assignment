package zavrsniTestAdmin;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniAdmin.LoginAdminPage;
import zavrsniAdmin.PocetnaPage;
import zavrsniAdmin.RegistracijaStanaraPage;
import zavrsniAdmin.StanariPage;
import zavrsniAdmin.ZgradePage;

public class RegistracijaStanaraTest {

	private WebDriver driver;
	private LoginAdminPage loginAdminPage;
	private String baseUrl;
	private PocetnaPage pocetnaPage;
	private ZgradePage zgradePage;
	private StanariPage stanariPage;
	private RegistracijaStanaraPage registracijaStanaraPage;

	@BeforeClass
	public void setupSelenium() {

		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080";
		driver.navigate().to(baseUrl);

		loginAdminPage = new LoginAdminPage(driver);
		stanariPage = new StanariPage(driver);
		registracijaStanaraPage = new RegistracijaStanaraPage(driver);
		zgradePage = new ZgradePage(driver);
		pocetnaPage = new PocetnaPage(driver);

		loginAdminPage.login("admin@gmail.com", "Bar5slova");
		pocetnaPage.navigateToPageStanari();
	
	}
	
	@Test(priority = 0)
	public void pozitivanTestRegistracijeStanara() throws InterruptedException{
		
		registracijaStanaraPage.registracijaStanar("jovancica@gm.rs", "Bar6slova", "Jo", "Maj");
		String uspesnoRegistrovanStanarPoruka = registracijaStanaraPage.getUspesnaRegMsg();
		assertEquals(uspesnoRegistrovanStanarPoruka, "Uspesno ste registrovali stanara!");
		
	}
	
	@Test(priority = 1)
	public void negativanTestPraznaPoljal() throws InterruptedException{
		
		registracijaStanaraPage.registracijaStanar("", "", "", "");
		String obazveznaPoljaPoruka = registracijaStanaraPage.getPraznoPoljeMsg();
		assertTrue(true, obazveznaPoljaPoruka);
		Boolean vidljivostBtn = registracijaStanaraPage.getRegistracijaBtn().isEnabled();
		assertFalse(vidljivostBtn);
		
	}

	@Test(priority = 2)
	public void negativanTestFormatLozinke() throws InterruptedException{
		
		registracijaStanaraPage.registracijaStanar("kosta@ma.rs", "bar5slova", "Kosta", "Misic");
		String neispravanFormatLozinke = registracijaStanaraPage.getNevalidnaLozinkaMsg();
		assertTrue(true, neispravanFormatLozinke);
		Boolean vidljivostBtn = registracijaStanaraPage.getRegistracijaBtn().isEnabled();
		assertFalse(vidljivostBtn);
		
	}
	
	@Test(priority = 3)
	public void negativanTestZauzetEmail() throws InterruptedException{
		
		registracijaStanaraPage.registracijaStanar("marko@gmail.com", "Bar6slova", "Miki", "Maj");
		String zauzetMailPoruka = registracijaStanaraPage.getZauzetiEmailMsg();
		assertTrue(true, zauzetMailPoruka);
		
		
	}
	
	@Test(priority = 4)
	public void testResetBtn() throws InterruptedException{
		
		registracijaStanaraPage.resetPodataka("filip@ma.rs", "Bar5slova", "Filip", "Misic");
		
		String poljeEmail = registracijaStanaraPage.getEmailStanar().getText();
		String poljeLozinka = registracijaStanaraPage.getLozinkaStanar().getText();
		String poljeIme = registracijaStanaraPage.getImeStanar().getText();
		String poljePrezime = registracijaStanaraPage.getPrezimeStanar().getText();
		
		assertEquals(poljeEmail, "");
		assertEquals(poljeLozinka, "");
		assertEquals(poljeIme, "");
		assertEquals(poljePrezime, "");
	}

	@Test(priority = 5)
	public void testProveraRegistrovanogStanara() throws InterruptedException{
		
		stanariPage.getPregledBtn().click();
		Select dropdown = new Select(stanariPage.getPrikazStanara());
		dropdown.selectByValue("25");
		//assertEquals(stanariPage.getBrojPrikaza25(), "25");

		stanariPage.setSearchPolje("Jo Maj");
		stanariPage.getSearchBtn().click();
		
		boolean vrednost = stanariPage.isTextInTable("Marko Markovic");
		assertTrue(vrednost);
			
		}
	
	
}
