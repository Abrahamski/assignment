package zavrsniTestAdmin;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniAdmin.LoginAdminPage;
import zavrsniAdmin.PocetnaPage;
import zavrsniAdmin.ZgradePage;

public class DodavanjeZgradaTest {
	
	private WebDriver driver;
	private LoginAdminPage loginAdminPage;
	private String baseUrl;
	private PocetnaPage pocetnaPage;
	private ZgradePage zgradePage;
	
	@BeforeClass
	public void setupSelenium() {

		//System.setProperty("webdriver.gecko.driver", "geckodriver");
		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080/logovanje";
		driver.navigate().to(baseUrl);
		
		loginAdminPage = new LoginAdminPage(driver);
		pocetnaPage = new PocetnaPage(driver);
		zgradePage = new ZgradePage(driver);
		//registracijaPage = new RegistracijaPage(browser);
		loginAdminPage.login("admin@gmail.com", "Bar5slova");
		
		pocetnaPage.navigateToPageZgrade();
	}
	
	@Test(priority = 0)
	public void pozitivanTestDodavanjeZgrade() throws InterruptedException{
		
		zgradePage.dodajZgradu("Novi Sad", "Rumenacki put", "44", "12");
		
		String poruka = zgradePage.getUspesnoDodataZgradaMsg().getText();
		assertEquals(poruka, "Uspesno ste dodali zgradu!");
	}
	
	@Test(priority = 1)
	public void negativanTestDodavanjeZgradaPostoji() throws InterruptedException {
		
		zgradePage.dodajZgradu("Novi Sad", "Rumenacki put", "3", "24");
		
		String porukaZgradaPostoji = zgradePage.getZgradaPostojiMsg().getText();
		assertEquals(porukaZgradaPostoji, "Vec postoji zgrada na toj adresi!");
	}
	
	@Test(priority = 2)
	public void resetPolja() throws InterruptedException {
		
		zgradePage.getResetBtn().click();
		
		String poljeMesto = zgradePage.getMesto().getText();
		String poljeUlica = zgradePage.getUlica().getText();
		String poljeBroj = zgradePage.getBroj().getText();
		String poljeBrojStanova = zgradePage.getBrojStanova().getText();
		
		assertEquals(poljeMesto, "");
		assertEquals(poljeUlica, "");
		assertEquals(poljeBroj, "");
		assertEquals(poljeBrojStanova, "");
	}
	
	@Test(priority = 3)
	public void negativanTestPraznaPolja() throws InterruptedException {
		
		zgradePage.dodajZgradu("", "", "", "");
		
		String porukaPraznoPolje = zgradePage.getObaveznoPoljeMsg().getText();
		assertEquals(porukaPraznoPolje, "Ovo polje ne sme biti prazno!");
	}

	@Test(priority = 4)
	public void pregledZgrade() throws InterruptedException {
		
		zgradePage.getPregledBtn().click();
		zgradePage.setSearchPoljeUlicaBroj("Rumenacki put 99");
		zgradePage.getSearchBtn().click();
		
		boolean vrednost = zgradePage.isTextInTable("Rumenacki put 99");
		assertTrue(vrednost);
		
	}
}
