package zavrsniTestAdmin;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniAdmin.LoginAdminPage;
import zavrsniAdmin.PocetnaPage;
import zavrsniAdmin.RegistracijaStanaraPage;
import zavrsniAdmin.StanariPage;
import zavrsniAdmin.ZgradePage;

public class StanariTest {

	private WebDriver driver;
	private LoginAdminPage loginAdminPage;
	private String baseUrl;
	private PocetnaPage pocetnaPage;
	private ZgradePage zgradePage;
	private StanariPage stanariPage;
	private RegistracijaStanaraPage registracijaPage;

	@BeforeClass
	public void setupSelenium() {

		System.setProperty("webdriver.chrome.driver","c:\\Users\\Darko\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080";
		driver.navigate().to(baseUrl);

		loginAdminPage = new LoginAdminPage(driver);
		stanariPage = new StanariPage(driver);
		registracijaPage = new RegistracijaStanaraPage(driver);
		zgradePage = new ZgradePage(driver);
		pocetnaPage = new PocetnaPage(driver);

		loginAdminPage.login("admin@gmail.com", "Bar5slova");
		pocetnaPage.navigateToPageStanari();
	
	}
	
	@Test(priority = 0)
	public void testPregledStanara() throws InterruptedException{
		
		stanariPage.getPregledBtn().click();
		String stanariUrl = baseUrl + "/stanari/pregled";
		assertEquals(stanariUrl, "http://localhost:8080/stanari/pregled");
	
	}
	
	@Test(priority = 1)
	public void testPrikazStanara() throws InterruptedException{
		
		Select dropdown = new Select(stanariPage.getPrikazStanara());
		dropdown.selectByValue("10");
		assertEquals(stanariPage.getBrojPrikaza10(), "10");
	
	}
	
//	@Test (priority = 2)
//	public void brojStanaraTabela() throws InterruptedException {
//		//stanariPage.getPregledBtn().click();
//										
//		int count = stanariPage.getRedoviTabele().size();		
//		assertEquals(count, 5);
//
//	}
	
	@Test(priority = 2)
	public void pretragaStanara() throws InterruptedException {
		
		//stanariPage.getPregledBtn().click();
		stanariPage.setSearchPolje("Marko Markovic");
		stanariPage.getSearchBtn().click();
		
		boolean vrednost = stanariPage.isTextInTable("Marko Markovic");
		assertTrue(vrednost);
		
	}
	
	@Test (priority = 3)
	public void stanarOpcije() throws InterruptedException {
		
		stanariPage.getPregledBtn().click();
		//stanariPage.setSearchPolje("");
		stanariPage.getStanarByName("Gospodin Predsednik ").click();
		stanariPage.getNavigateToStanar();
		String stanarUrl = baseUrl + "/stanar/2";
		assertTrue(true, stanarUrl);
		
		stanariPage.getZgradaLink().click();
		stanariPage.getNavigateToZgrada();
		String zgradaUrl = baseUrl + "/zgrada/1/stanovi";
		assertTrue(true, zgradaUrl);
		
	}	
	
	@AfterClass
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
	}
	
	
	

	
}
