package zavrsniTestAdmin;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import zavrsniAdmin.LoginAdminPage;
import zavrsniAdmin.PocetnaPage;
import zavrsniAdmin.ZgradePage;

public class PregledZgradeTest {

	private WebDriver driver;
	private LoginAdminPage loginAdminPage;
	private String baseUrl;
	private PocetnaPage pocetnaPage;
	private ZgradePage zgradePage;
	
	@BeforeClass
	public void setupSelenium() {

		//System.setProperty("webdriver.gecko.driver", "geckodriver");
		System.setProperty("webdriver.chrome.driver","d:\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		baseUrl = "http://localhost:8080/logovanje";
		driver.navigate().to(baseUrl);
		
		loginAdminPage = new LoginAdminPage(driver);
		pocetnaPage = new PocetnaPage(driver);
		zgradePage = new ZgradePage(driver);
		//registracijaPage = new RegistracijaPage(browser);
		loginAdminPage.login("admin@gmail.com", "Bar5slova");
		
		zgradePage.navigateToPregledZgrade();
	}
	
	@Test(priority = 0)
	public void testPrikaz25() throws InterruptedException {
		//testiranje drop elementa
		Select dropdown = new Select(zgradePage.getPrikaz());
		dropdown.selectByValue("25");
		assertEquals(zgradePage.getBrojPrikaza25(), "25");
	}
	
	@Test(priority = 1)
	public void testProveraDodavanjaZgrade () throws InterruptedException {
		
		zgradePage.getPregledBtn().click();
		// da li zgrada postoji u tabeli
		WebElement zgradaRow = zgradePage.getZgradaByName("Boska Buhe 42");
		String rowData = zgradaRow.getText();
		assertTrue(rowData.contains("Boska Buhe 42"));
	
	}
	
	@Test(priority = 2)
	public void testSelektujZgradu() throws InterruptedException {
		
		zgradePage.getZgradaByName("Boska Buhe 42").click();
		String zgradaUrl = baseUrl + "/zgrada/1/stanovi";
		assertTrue(true, zgradaUrl);
		
	}
	
	@Test(priority = 3)
	public void testPrikazStanova50() throws InterruptedException {
		
		Select dropdown = new Select(zgradePage.getPrikazStanova());
		dropdown.selectByValue("50");
		assertEquals(zgradePage.getPrikazStanova50(), "50");
	}
	
	@Test(priority = 4)
	public void testDodajStanara() throws InterruptedException {
		
		//zgradePage.getTabelaZgrada().findElement(By.xpath("//a[@href='/zgrada/1']")).click();
		zgradePage.getTabelaStanovi().findElement(By.xpath("//a[@href='/stan/1']")).click();
		String urlStanar = baseUrl + "/stan/1";
		assertTrue(true, urlStanar);
		
		zgradePage.getDodajStanareBtn().click();
		String uspesnoDodatStanarPoruka = "Uspesno ste dodali stanara!";
		assertEquals(uspesnoDodatStanarPoruka, zgradePage.getUspesnoDodatStanarMsg());

		zgradePage.getPostaviPredsednikaBtn().click();
		Assert.assertFalse(zgradePage.getPostaviPredsednikaBtn().isEnabled());
	}
	
	
	@Test(priority = 5)
	public void testDodajVlasnika() throws InterruptedException {
//		
//		zgradePage.getTabelaZgrada().findElement(By.xpath("//a[@href='/zgrada/1']")).click();
//		zgradePage.getTabelaStanovi().findElement(By.xpath("//a[@href='/stan/1']")).click();
//		
		zgradePage.getPostaviVlasnikaBtn().click();
		String stvarniVlasnik = zgradePage.getNaslovVlasnik();
		String naslovVlasnik = "Vlasnik: Nema vlasnika";
		assertNotEquals(stvarniVlasnik, naslovVlasnik);
		//assertEquals(ocekivanaPoruka, zgradePage.getUspesnoDodatVlasnikMsg());
		//assertEquals(ocekivanaPoruka, zgradePage.getUspesnoDodatVlasnikMsg());
	}
	
	@Test(priority = 6)
	public void testUkloniStanara() throws InterruptedException {
		
		zgradePage.getUkloniStanaraBtn().click();
		String uspesnoUklonjenStanarPoruka = "Uspesno ste uklonili stanara!";
		assertEquals(uspesnoUklonjenStanarPoruka, zgradePage.getUspesnoUklonjenStanarMsg());

//		zgradePage.getPostaviPredsednikaBtn().click();
//		Assert.assertFalse(zgradePage.getPostaviPredsednikaBtn().isEnabled());
	}
	
	@Test(priority = 7)
	public void testUkloniVlasnika() throws InterruptedException {
		
		zgradePage.getTabelaZgrada().findElement(By.xpath("//a[@href='/zgrada/1']")).click();
		zgradePage.getTabelaStanovi().findElement(By.xpath("//a[@href='/stan/5']")).click();
		
		zgradePage.getUkloniVlasnikaBtn().click();
		String uspesnoUklonjenVlasnikPoruka = "Uspesno ste uklonili vlasnika!";
		assertEquals(uspesnoUklonjenVlasnikPoruka, zgradePage.getUspesnoUklonjenVlasnikMsg());

	}
	
	@Test(priority=8)
	public void zgradaFunkcijeTest() throws InterruptedException {
		
		zgradePage.getPregledBtn().click();
		zgradePage.getTabelaZgrada().findElement(By.xpath("//a[@href='/zgrada/1']")).click();
		zgradePage.getObavestenje().click();
		String oabvestenjeUrl = baseUrl + "/zgrada/1/obavestenja";
		assertTrue(true, oabvestenjeUrl);
		
		zgradePage.getPredloziTackeDnevnogReda().click();
		String tackeDevnogRedaUrl = baseUrl + "/zgrada/1/tacke";
		assertTrue(true, tackeDevnogRedaUrl);
				
		zgradePage.getSastanciSkupstine().click();
		Select dropdown = new Select(zgradePage.getFilter());
	//	dropdown.selectByValue("2: 1");
		dropdown.selectByVisibleText("Sastanci u toku");
//		zgradePage.getPregledajTacke().click();
		
		zgradePage.getSastanciSkupstine().click();
		Select dropdownProsli = new Select(zgradePage.getFilter());
		//dropdownProsli.selectByValue("3: 2");
		dropdownProsli.selectByVisibleText("Prosli Sastanci");
		//zgradePage.getZapisnik().click();	
		//zgradePage.getAnketa().click();
//		String anketaUrl = baseUrl + "/zgrada/1/sastanci/3/tacka/12/anketa";
//		assertTrue(true, anketaUrl);
	}
	
	@Test(priority=9)
	public void zgradaKvaroviTest() throws InterruptedException {
		
		zgradePage.getPregledBtn().click();
		zgradePage.getTabelaZgrada().findElement(By.xpath("//a[@href='/zgrada/1']")).click();
		//zgradaPage.getzutaOpcija().click();
		zgradePage.getKvarovi().click();
		zgradePage.getCheckbox().click();
		
	}
	
	@AfterClass
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
		
	}

}
