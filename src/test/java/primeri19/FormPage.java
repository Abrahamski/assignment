package primeri19;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormPage {
	WebDriver driver;

	public FormPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement getValidateBtn(){
		return (new WebDriverWait(driver,15))
		.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()=\"Submit\"]")));
//		 driver.findElement(By.xpath("//button[text()=\"Validate\"]"));
	}
	
	public WebElement getUsername(){
		return (new WebDriverWait(driver,15))
				.until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
//		return driver.findElement(By.name("username"));
	}
	
	public void setUsername(String value){
		WebElement inpt = getUsername();
		inpt.clear();
		inpt.sendKeys(value);
	}
	
	public WebElement getRequiredMsg(String field){
		return driver.findElement(By.xpath("//input[@name=\"" + field + "\"]/following-sibling::div/div[@data-validator=\"notEmpty\"]"));
	}
	
	
	
	
	public WebElement getUsernameRequiredMsg(){
		return driver.findElement(By.xpath("//input[@name=\"username\"]/following-sibling::div/div[@data-validator=\"notEmpty\"]"));
	}
	
	public WebElement getPasswordRequiredMsg(){
		return driver.findElement(By.xpath("//input[@name=\"password\"]/following-sibling::div/div[@data-validator=\"notEmpty\"]"));
	}
	public WebElement getFirstNameRequiredMsg(){
		return driver.findElement(By.xpath("//input[@name=\"firstName\"]/following-sibling::div/div[@data-validator=\"notEmpty\"]"));
	}
	
	public WebElement getUsernameRequiredPattern(){
		return driver.findElement(By.xpath("//input[@name=\"username\"]/following-sibling::div/div[@data-validator=\"stringLength\"]"));
	}
	
	
	
	
	public WebElement getFirstName(){
		return driver.findElement(By.name("firstName"));
	}
	
	
	
	
}
