package primeri19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PrimerValidacijaFormi {
	public static String getNCharacters(int n) {
		StringBuffer outputBuffer = new StringBuffer(n);
		for (int i = 0; i < n; i++) {
			outputBuffer.append("a");
		}
		return outputBuffer.toString();
	}

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "chromedriver");
		WebDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.get("https://formvalidation.io/download/");
		WebDriver frame = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@title='Demo']")));

		FormPage formPage = new FormPage(frame);

		// TODO 1 validate form elements visibility
		System.out.println(formPage.getUsername().isDisplayed());
		WebElement validateBtn = formPage.getValidateBtn();
		validateBtn.click();
		System.out.println(formPage.getUsernameRequiredMsg().getText());
//		System.out.println(formPage.getUsernameRequiredPattern().isDisplayed());
		System.out.println(validateBtn.isEnabled());
		
		formPage.setUsername(getNCharacters(5));

//		System.out.println(formPage.getUsernameRequiredMsg().isDisplayed());
		System.out.println(formPage.getUsernameRequiredPattern().getText());
		System.out.println(validateBtn.isEnabled());

		// TODO 2 validate all fields error messages
		driver.close();
	}
}
