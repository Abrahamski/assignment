package task3Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.junit.Assert;


import task3Page.Task3Page;

import task3Page.Task3Page;

public class Task3Test {

	private WebDriver driver;
	private Task3Page task3Page;
	private String baseUrl;
	
	@BeforeClass
	public void setupSelenium() {
		
		System.setProperty("webdriver.gecko.driver", "D:\\geckodriver.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		WebDriver driver = new FirefoxDriver(capabilities);
		baseUrl = "https://orangehrm-demo-7x.orangehrmlive.com/";
		driver.navigate().to(baseUrl);
		
		task3Page = new Task3Page(driver);
		
	}
	
	@Test(priority = 0)
	public void searchOption() throws InterruptedException {
		task3Page.clickBtn();
		Thread.sleep(5000);
		//wait to page be loaded
		/*new WebDriverWait(driver, pageLoadTimeout).until(
			      webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
		}	*/	
		String actualTitle = task3Page.getTitle().getText();
		Assert.assertEquals("Dashboard", actualTitle);
		
	}
	
	@Test(priority = 1 )
	public void goToRecruitmentPage() throws InterruptedException {
		task3Page.clickRecruitment();
		task3Page.clickCandidate();
		Thread.sleep(5000);
		String candidatesUrl = baseUrl + "client/#/noncore/recruitment/viewCandidates";
		Assert.assertEquals("https://orangehrm-demo-7x.orangehrmlive.com/client/#/noncore/recruitment/viewCandidates", candidatesUrl);
	}
	
	@Test(priority = 2 )
	public void countAllCandidates() throws InterruptedException {
		String result = task3Page.getNumberOfCandidates().getText().split(" ")[2];
		int cResult = Integer.parseInt(result);
		System.out.println("Number of candidates " + cResult);
		Thread.sleep(5000);
		int expectedNumberOfCandidates = 44;
		Assert.assertEquals(cResult, expectedNumberOfCandidates);
	}
	
	@Test(priority = 3)
	public void addCandidate() throws InterruptedException {
		task3Page.clickOnAddBtn();
		Thread.sleep(5000);
		String titleAddCandidate = task3Page.getCustomHeader().getText();
		Assert.assertEquals("Add Candidate", titleAddCandidate);
	}
	
	//Coudln't finish assignment: Account disabled  27.08.2021. before 8:00
	
	
	
}
