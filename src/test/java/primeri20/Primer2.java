package primeri20;

import static org.testng.AssertJUnit.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import rs.ac.uns.testdevelopment.ssluzba.pages.IspitniRokoviCreationPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.IspitniRokoviPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.LoginPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.MenuPage;
import rs.ac.uns.testdevelopment.ssluzba.pages.ModalDeletePage;

@Test
public class Primer2 {
	private WebDriver driver;
	private LoginPage loginPage;
	private MenuPage menuPage;
	private IspitniRokoviPage ispitniRokoviPage;
	private ModalDeletePage modalDeletePage;
	private IspitniRokoviCreationPage ispitniRokoviCreationPage;

	@BeforeMethod
	public void setupSelenium() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(1024, 768));
		driver.navigate().to("localhost:8080/#/");

		// init pages
		loginPage = new LoginPage(driver);
		menuPage = new MenuPage(driver);
		modalDeletePage = new ModalDeletePage(driver);
		ispitniRokoviPage = new IspitniRokoviPage(driver);
		ispitniRokoviCreationPage = new IspitniRokoviCreationPage(driver);
	}

	public void studentCreation() throws InterruptedException {
		menuPage.getAccountMenu().click();
		AssertJUnit.assertEquals(true, menuPage.getSignUp().isDisplayed());
		menuPage.getSignUp().click();
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.titleIs("Sign in"));
		AssertJUnit.assertEquals("http://localhost:9000/#/login", driver.getCurrentUrl());
		loginPage.login("admin", "admin");

		menuPage.getEntities().click();
		assertTrue(menuPage.getStudentsLink().isDisplayed());
		menuPage.getIspitniRokoviLink().click();
		
		ispitniRokoviPage.getCreateBtn().click();
		ispitniRokoviCreationPage.createIspitniRok("Novi", "2016-06-08", "2016-06-30");

	}

	@AfterMethod
	public void closeSelenium() {
		// Shutdown the browser
		driver.quit();
	}

}
