package task1Test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.remote.DesiredCapabilities;


import task1Page.Task1Page;

public class Task1Test {
	
	private WebDriver driver;
	private Task1Page task1Page;
	private String baseUrl;
	
	@BeforeClass
	public void setupSelenium() {
		
		System.setProperty("webdriver.gecko.driver", "geckodriver");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		WebDriver driver = new FirefoxDriver(capabilities);
		baseUrl = "https://www.google.com/";
		driver.navigate().to(baseUrl);
		
		task1Page = new Task1Page(driver);
		
	}
	
	@Test(priority = 0)
	public void searchOption() throws InterruptedException {
		task1Page.setSearch("demoqa");
		Thread.sleep(5000);
		
	}
	
	@Test(priority = 1)
	public void choseFirstResult() throws InterruptedException {
		 List<WebElement> list = driver.findElements(By.xpath("//*[@id='rso']/div[1]/div/div/div/div/div/div[1]/a/h3"));
		 	for (int i=0; i<list.size(); i++) {
		 		list.get(0).click();
		 		break;
		 	}
	}
	//task1Page.getResult();
}
